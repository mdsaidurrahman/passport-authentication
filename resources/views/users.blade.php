@extends('main')
@section('content')
    <section class="vh-100 gradient-custom">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card bg-dark text-white" style="border-radius: 1rem;">
                        <div class="card-body p-5 text-center">

                            <div class="mb-md-3 mt-md-2 pb-2">

                                <h2 class="fw-bold mb-2 text-uppercase">All Users</h2>
                                <div class="" id="user">

                                </div>
                            </div>

                            <div id="users">
                                {{-- <button class="btn btn-primary" >Name: ${user.name}</button> --}}
                            </div>
                            <div class="" id="logout">
                                <button class="btn btn-primary" onclick="logout()">Logout</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        userLogin()

        function userLogin() {

            $.ajax({
                type: "get",
                url: "/api/users",
                success: function(response) {
                    if (response.status == 'success') {
                        var allUser = response.users;
                        // console.log(usersId);
                        allUser.forEach(user => {
                            $('#users').append(
                                `<button onclick="singleUser(this)" id="${user.id}" class="btn btn-primary m-2">Name: ${user.name}</button>`
                            );
                        });
                    } else {
                        alert(response)
                    }

                }
            });


        }

        function singleUser(button) {
            var getToken = localStorage.getItem('token');
            var userId = button.id;

            $.ajax({
                type: "get",
                url: `/api/user/${button.id}`,
                headers: {
                    'Authorization': 'Bearer ' + getToken,
                    'Accept': 'application/json',
                },
                success: function(response) {
                    console.log(response);
                    if (response.status == 'success') {
                        var user = response.user;
                        // console.log(usersId);
                        $('#user').html(
                            `<p>Name: ${user.name}</p>
                                    <p>Email: ${user.email}</p></button>`
                        );
                    } else if (response.message) {
                        alert(response.message)
                    }
                    else{
                        alert(response)
                    }

                },error: function(xhr, status, error) {
                    // Handle errors if any
                    alert(xhr.statusText);
                }
            });

        }

        function logout() {
            var getToken = localStorage.getItem('token');
            $.ajax({
                type: "POST",
                url: "/api/logout",
                headers: {
                    'Authorization': 'Bearer ' + getToken, 
                    'Accept': 'application/json'
                },
                success: function(response) {
                    if (response.status == 'success') {
                        localStorage.clear();
                        window.location.href = '/login'; 
                    } else if (response.status == 'error') {
                        alert(response.message);
                    }
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                    alert('An error occurred while logging out.');
                }
            });
        }
    </script>
@endsection
