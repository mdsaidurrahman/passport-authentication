<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserController extends Controller
{
    /**
     * Displays the login page.
     */
    function loginPage()
    {
        return view('login');
    }

    /**
     * Displays the sign up page.
     */
    function signUpPage()
    {
        return view('signUp');
    }

    /**
     * Displays the users page.
     */
    function usersPage()
    {
        return view('users');
    }

    /**
     * Retrieves all users and returns a JSON response.
     */
    function users()
    {
        $users = User::all();
        return response()->json([
            'status' => 'success',
            'users' => $users
        ]);
    }

    /**
     * Retrieves a specific user by ID and returns a JSON response.
     *
     * @param int $id The ID of the user to retrieve.
     * @return \Illuminate\Http\JsonResponse
     */
    function user($id)
    {
        $loginUserId = auth()->user()->id;
        $user = User::findOrFail($id);
        if ($loginUserId == $user->id) {
            return response()->json([
                'status' => 'success',
                'user' => $user,
                'login' => $loginUserId
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'You are not authorized to access this resource'
            ]);
        }
    }
}